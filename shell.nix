let
  unstable = import <unstable> {};
in
{pkgs ? import <nixpkgs> {}, ...}: pkgs.mkShell {
  name = "TAP Webclient environment";
  src = ./.;
  packages = with unstable; [
    nodejs
    nodePackages.pnpm
    jq
    yq
  ];

  shellHook = ''
    export PATH="$PATH:$(pwd)/node_modules/.bin"
  '';
}
